package brain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Converter {
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public String convertToCentimeter(String value, String unit){
		double result = 0.0;
		switch (unit){
			case "Yard":
				result = Double.parseDouble(value) / 0.010936;
				break;
			case "Zoll":
				result = Double.parseDouble(value) / 0.39370;
				break;
			case "Fuß":
				result = Double.parseDouble(value) / 0.032808;
				break;
			case "Zentimeter":
				result = Double.parseDouble(value);
			default:
				break;
		}
		
		return String.valueOf(round(result, 2));
	}
	
	public String convertToYards(String value, String unit){
		double result = 0.0;
		switch (unit){
			case "Yard":
				result = Double.parseDouble(value);
				break;
			case "Zoll":
				result = Double.parseDouble(value) * 0.027778;
				break;
			case "Fuß":
				result = Double.parseDouble(value) * 0.33333;
				break;
			case "Zentimeter":
				result = Double.parseDouble(value) * 0.010936;
			default:
				break;
		}
		
		return String.valueOf(round(result, 2));
	}
	
	public String convertToFeet(String value, String unit){
		double result = 0.0;
		switch (unit){
			case "Yard":
				result = Double.parseDouble(value) * 3.0000;
				break;
			case "Zoll":
				result = Double.parseDouble(value) * 0.083333;
				break;
			case "Fuß":
				result = Double.parseDouble(value);
				break;
			case "Zentimeter":
				result = Double.parseDouble(value) * 0.0328084;
			default:
				break;
		}
		
		return String.valueOf(round(result, 2));
	}
	
	public String convertToInch(String value, String unit){
		double result = 0.0;
		switch (unit){
			case "Yard":
				result = Double.parseDouble(value) * 36.000;
				break;
			case "Zoll":
				result = Double.parseDouble(value);
				break;
			case "Fuß":
				result = Double.parseDouble(value) * 12.000;
				break;
			case "Zentimeter":
				result = Double.parseDouble(value) * 0.393701;
			default:
				break;
		}
		
		return String.valueOf(round(result, 2));
	}
	

}
