package gui;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.DocumentFilter.FilterBypass;
import javax.swing.text.PlainDocument;

import brain.Converter;
import brain.EmailValidator;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class JFrameCalc extends JFrame {

	private JPanel contentPane;
    private JFormattedTextField txtValue;
	private JTextField textFieldEMail;
	
	private double value;
	
    //Formats to format and parse numbers
    private NumberFormat valueFormat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrameCalc frame = new JFrameCalc();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrameCalc() {
		Converter converter = new Converter();
		
		setResizable(false);
		setTitle("Längenumrechner");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 342);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JComboBox comboBoxFrom = new JComboBox();
		comboBoxFrom.setModel(new DefaultComboBoxModel(new String[] {"Zoll", "Yard", "Fuß", "Zentimeter"}));
		comboBoxFrom.setSelectedIndex(0);
		comboBoxFrom.setBounds(6, 79, 148, 27);
		contentPane.add(comboBoxFrom);
			
	    txtValue = new JFormattedTextField(valueFormat);
	    txtValue.setValue(new Double(value));
	    txtValue.setColumns(10);
		txtValue.setHorizontalAlignment(SwingConstants.CENTER);
		txtValue.setBounds(87, 23, 134, 28);
		contentPane.add(txtValue);
		
		JLabel lblVon = new JLabel("Von:");
		lblVon.setBounds(6, 63, 74, 16);
		contentPane.add(lblVon);
		
		JLabel lblZu = new JLabel("Zu:");
		lblZu.setBounds(179, 63, 61, 16);
		contentPane.add(lblZu);
		
		JComboBox comboBoxTo = new JComboBox(new Object[]{});
		comboBoxTo.setModel(new DefaultComboBoxModel(new String[] {"Zoll", "Yard", "Fuß", "Zentimeter"}));
		comboBoxTo.setSelectedIndex(1);
		comboBoxTo.setBounds(156, 79, 138, 27);
		contentPane.add(comboBoxTo);
		
		JLabel lblErgebnis = new JLabel("");
		lblErgebnis.setHorizontalAlignment(SwingConstants.CENTER);
		lblErgebnis.setBounds(6, 185, 288, 16);
		contentPane.add(lblErgebnis);
		
		JButton btnUmrechnen = new JButton("Umrechnen");
		btnUmrechnen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Check if TextField is empty
				if (txtValue.getValue() == null){
					lblErgebnis.setText("Keine Eingabe!");
					return;
				}
				switch ((String)comboBoxTo.getSelectedItem()){
				case "Yard":
					lblErgebnis.setText(converter.convertToYards(txtValue.getText(), (String)comboBoxFrom.getSelectedItem()) + " Yards");
					break;
				case "Zoll":
					lblErgebnis.setText(converter.convertToInch(txtValue.getText(), (String)comboBoxFrom.getSelectedItem()) + " Zoll");
					break;
				case "Fuß":
					lblErgebnis.setText(converter.convertToFeet(txtValue.getText(), (String)comboBoxFrom.getSelectedItem()) + " Fuß");
					break;
				case "Zentimeter":
					lblErgebnis.setText(converter.convertToCentimeter(txtValue.getText(), (String)comboBoxFrom.getSelectedItem()) + " cm");
					break;
				}
			}
		});
		btnUmrechnen.setBounds(87, 135, 134, 29);
		contentPane.add(btnUmrechnen);
		
		textFieldEMail = new JTextField();
		textFieldEMail.setColumns(10);
		textFieldEMail.setBounds(117, 228, 177, 28);
		contentPane.add(textFieldEMail);
		
		JLabel lblEmailadresse = new JLabel("E-Mail-Adresse:");
		lblEmailadresse.setBounds(6, 234, 111, 16);
		contentPane.add(lblEmailadresse);
		
		JLabel lblEmailStatus = new JLabel("");
		lblEmailStatus.setHorizontalAlignment(SwingConstants.CENTER);
		lblEmailStatus.setFont(new Font("Lucida Grande", Font.PLAIN, 9));
		lblEmailStatus.setBounds(6, 298, 288, 16);
		contentPane.add(lblEmailStatus);
		
		JButton btnSchicken = new JButton("Schicken");
		btnSchicken.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Check if Email address is valid
				EmailValidator emailValidator = new EmailValidator();
				   if(!emailValidator.validate(textFieldEMail.getText().trim())) {
					   System.out.println("Invalid Email ID");
					   lblEmailStatus.setText("Ugültige E-Mail-Adresse");
				   } else {
					   lblEmailStatus.setText("Erfolgreich gesendet");
				   }
			}
		});
		btnSchicken.setBounds(87, 268, 134, 29);
		contentPane.add(btnSchicken);
		

	}
}
